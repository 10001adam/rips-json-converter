package jsonconverter;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import jsonconverter.Finding;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Main {

    public static void main(String [] args) {
        String path = args[0];

        Document htmlFile = null;
        try {
            htmlFile = Jsoup.parse(new File(path), "ISO-8859-1");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } // right
        String title = htmlFile.title();
        List<Element> fileboxes = htmlFile.getElementsByClass("filebox");
        List<Finding> findings = new ArrayList<>();

        for (Element filebox : fileboxes) {
            String filename = filebox.getElementsByClass("filename").first().text();
            filename = filename.replace("File: ", "");
            Element vulnerability = filebox.getElementById(filename);
            Elements allcats = vulnerability.getElementsByAttributeValue("name", "allcats");
            for (Element allcat : allcats) {
                Finding finding = new Finding();
                finding.setFile_path(filename);
                finding.setTitle(allcat.getElementsByClass("vulnblock").first().attr("name"));
                finding.setDescription(allcat.getElementsByClass("vulntitle").first().text());
                finding.setSeverity("High"); // all of the vulnerabilities RISP can discover are considered HIGH severity by other tools
                Elements linenrs = allcat.getElementsByClass("linenr");
                String allLines = "\nAll lines: ";
                for (Element linenr : linenrs) {
                    allLines += (" " + linenr.text() + " ");
                }
                finding.setDescription(finding.getDescription() + allLines);
                findings.add(finding);
            }
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(findings,
                new TypeToken<Collection<Finding>>() {
                }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        JsonObject object = new JsonObject();
        object.add("findings", jsonArray);
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(path.replace("html", "json"));
            byte[] strToBytes = object.toString().getBytes();
            outputStream.write(strToBytes);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();


        }
    }
}

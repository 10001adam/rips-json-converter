package jsonconverter;

public class Finding {
    private String title;
    private String description;
    private String severity;
    private String date;
    private String cve;
    private String cwe;
    private String cvssv3;
    private String file_path;
    private int line;

    public Finding(String title, String description, String severity, String date, String cve, String cwe, String cvssv3, String file_path, int line) {
        this.title = title;
        this.description = description;
        this.severity = severity;
        this.date = date;
        this.cve = cve;
        this.cwe = cwe;
        this.cvssv3 = cvssv3;
        this.file_path = file_path;
        this.line = line;
    }

    public Finding() {
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setCve(String cve) {
        this.cve = cve;
    }

    public void setCwe(String cwe) {
        this.cwe = cwe;
    }

    public void setCvssv3(String cvssv3) {
        this.cvssv3 = cvssv3;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getSeverity() {
        return severity;
    }

    public String getDate() {
        return date;
    }

    public String getCve() {
        return cve;
    }

    public String getCwe() {
        return cwe;
    }

    public String getCvssv3() {
        return cvssv3;
    }

    public String getFile_path() {
        return file_path;
    }

    public int getLine() {
        return line;
    }
}

